package com.example.lesson4.profile.addresses

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.example.lesson4.R
import com.example.lesson4.profile.achivments.UserAddresses
import kotlinx.android.synthetic.main.add_new_address_userprofile.*

class AddNewAdressFromUser: Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_new_address_userprofile)

        iv_btn_back.setOnClickListener { back() }

        btnAddNewAddress.setOnClickListener { validFields() }

    }

    //функция  при клике  назад
    fun back(){
        val intent = Intent(this, UserAddresses::class.java)
        startActivity(intent)
    }

    //функция  валидавции заполнения полей
    fun validFields(){
        if(etAddNewPhone.text.isEmpty() || etAddNewFullAddress.text.isEmpty() ||
            etAddNewEntrance.text.isEmpty() || etAddNewFloor.text.isEmpty() || etAddNewFlat.text.isEmpty())
        {
            Toast.makeText(this, "Заполните все  поля!", Toast.LENGTH_SHORT).show()
        }
        else{
            //код при успешном  заполнении полей
        }
    }
}