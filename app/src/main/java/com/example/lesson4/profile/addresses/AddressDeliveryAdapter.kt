package com.example.lesson4.profile.addresses

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.lesson4.R

class AddressDeliveryAdapter(listArray:ArrayList<AddressDeliveryItem>, context: Context)
    :RecyclerView.Adapter<AddressDeliveryAdapter.ViewHolder>() {

    var listArrayR = listArray
    var contextR = context
    var imgDelete: ((AddressDeliveryItem, position:Int) -> Unit)? = null //для удаления объекта в recyclerView
    var imEdit: ((AddressDeliveryItem, position:Int) -> Unit)? = null //для равки объекта в recyclerView

    inner class ViewHolder(view:View):RecyclerView.ViewHolder(view) {
        var tvfullStreetName = view.findViewById<TextView>(R.id.tvFullNameStreet)
        var tvEntranceNumb = view.findViewById<TextView>(R.id.tvEntranceNumb)
        var tvFloorNumb = view.findViewById<TextView>(R.id.tvFloorNumb)
        var tvFlatNumb = view.findViewById<TextView>(R.id.tvFullFlatNumb)
        var tvFullPhoneNumb = view.findViewById<TextView>(R.id.tvFullPhoneNumb)

        //для операций удаления и  правки
        var ivDelete = view.findViewById<ImageView>(R.id.ivDelete)
        var ivEdit = view.findViewById<ImageView>(R.id.ivEditAddressItem)

        //для удаления
        init {
                ivDelete.setOnClickListener {
                    imgDelete?.invoke(listArrayR[adapterPosition],adapterPosition)
                }
        }

        //для редактирования
        init {
            ivEdit.setOnClickListener {
                imEdit?.invoke(listArrayR[adapterPosition],adapterPosition)
            }
        }

        fun bind(listItem: AddressDeliveryItem, context: Context){
            tvfullStreetName.text = listItem.fullAdress
            tvEntranceNumb.text = listItem.entranceNumb
            tvFloorNumb.text = listItem.floorNumb
            tvFlatNumb.text = listItem.flatNumb
            tvFullPhoneNumb.text = listItem.PhoneNumb
        }
    }
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        var inflater = LayoutInflater.from(contextR)
        return ViewHolder(inflater.inflate(R.layout.address_item, parent, false))
    }


    override fun getItemCount(): Int {
        return listArrayR.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var listItem = listArrayR.get(position)
        holder.bind(listItem,contextR)
    }

    //удаление  элемента
    fun removeItem(position: Int) {
        listArrayR.removeAt(position)
        notifyItemRemoved(position)
    }

}