package com.example.lesson4.profile.achivments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.lesson4.R
import com.example.lesson4.profile.MainActivity
import com.example.lesson4.profile.addresses.AddNewAdressFromUser
import com.example.lesson4.profile.addresses.AddressDeliveryAdapter
import com.example.lesson4.profile.addresses.AddressDeliveryItem
import com.example.lesson4.profile.addresses.EditAddressUser
import kotlinx.android.synthetic.main.user_addreses.*

class UserAddresses: Activity() {

    val listAddressDelivery = ArrayList<AddressDeliveryItem>()
    val adapter = AddressDeliveryAdapter(
        listAddressDelivery,
        this
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.user_addreses)

        //вызов функции добавления элементов массива  дял  адаптера "Адреса  доставки"
        addToAddressDeliveryArrayAdapter()
        //инициализация и  настрока Recycler для "Адреса доставки"
        rcViewAddrDelivery.adapter = adapter
//        rcViewAddrDelivery.hasFixedSize()
        rcViewAddrDelivery.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false)


        //для удаление на соответствующий  объект в recyclerView
        adapter.imgDelete = {it,id->
            adapter.removeItem(id)
        }

        //для правок("редактирования") в  объекте RecyclerView
        adapter.imEdit = {it,id ->
            var intent = Intent(this, EditAddressUser::class.java)
            intent.putExtra("phone", "${it.PhoneNumb}")
            intent.putExtra("address", "${it.fullAdress}")
            intent.putExtra("entrance", "${it.entranceNumb}")
            intent.putExtra("floor", "${it.floorNumb}")
            intent.putExtra("flat", "${it.flatNumb}")
            startActivity(intent)
        }
        //кнопка  назад
        iv_btn_back.setOnClickListener { back() }

        //кнопка  плюсик
        fabAddNewAddress.setOnClickListener { gotoAddAdress() }
    }

    fun back(){
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    //Добавление  массива в Адреса  доставки для  адаптера
    fun addToAddressDeliveryArrayAdapter(){
        listAddressDelivery.add(
            AddressDeliveryItem(
                1, "ул.Алиханова, 8", "5", "9",
                "150", "+7 705 256 54 84"
            )
        )
        listAddressDelivery.add(
            AddressDeliveryItem(
                2, " пр. Жамбыла 46 аптека Здоровье", "1", "4",
                "46", "+7 708 326 60 56"
            )
        )
        listAddressDelivery.add(
            AddressDeliveryItem(
                3, " ул. Ермекова 55", "10", "6",
                "3", "+7 702 357 30 21"
            )
        )
        listAddressDelivery.add(
            AddressDeliveryItem(
                4, " ул. Сатыбалдина 19", "4", "223",
                "9", "+7 702 111 30 77"
            )
        )
        listAddressDelivery.add(
            AddressDeliveryItem(
                5, " ул. Мельничная 120", "30", "128",
                "7", "+705 66 88 00 00"
            )
        )
        listAddressDelivery.add(
            AddressDeliveryItem(
                5, " ул. Открытая 34", "30", "128",
                "7", "+705 66 88 00 00"
            )
        )
        listAddressDelivery.add(
            AddressDeliveryItem(
                5, " ул. Кривогуза 41", "30", "128",
                "7", "+705 66 88 00 00"
            )
        )
    }

    //функция  перехода  на  создание адреса
    fun gotoAddAdress(){
        val intent = Intent(this, AddNewAdressFromUser::class.java)
        startActivity(intent)
    }

}