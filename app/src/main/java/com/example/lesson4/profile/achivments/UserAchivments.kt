package com.example.lesson4.profile.achivments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.lesson4.R
import com.example.lesson4.profile.MainActivity
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.achivments_user.*

class UserAchivments: Activity(), NavigationView.OnNavigationItemSelectedListener {
    var list = ArrayList<AchiveItem>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.achivments_user)
        iv_btn_back.setOnClickListener { back() }
        addItemList()
        rcView_achivments.adapter =
            AchiveAdapter(list, this)
        rcView_achivments.hasFixedSize()
        rcView_achivments.layoutManager = LinearLayoutManager(this)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        return true
    }

    fun back(){
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
    fun addItemList(){
        list.add(
            AchiveItem(
                R.drawable.ic_trueth,
                "Органический трафик стал нашим флагом в борьбе с ложью!"
            )
        )
        list.add(
            AchiveItem(
                R.drawable.ic_achive_car_star,
                "Органический трафик стал нашим флагом в борьбе с ложью!"
            )
        )
        list.add(
            AchiveItem(
                R.drawable.ic_achive_car_road,
                "Органический трафик стал нашим флагом в борьбе с ложью!"
            )
        )
        list.add(
            AchiveItem(
                R.drawable.ic_trueth,
                "Органический трафик стал нашим флагом в борьбе с ложью!"
            )
        )
        list.add(
            AchiveItem(
                R.drawable.ic_achive_car_star,
                "Органический трафик стал нашим флагом в борьбе с ложью!"
            )
        )
        list.add(
            AchiveItem(
                R.drawable.ic_achive_car_road,
                "Органический трафик стал нашим флагом в борьбе с ложью!"
            )
        )
    }
}