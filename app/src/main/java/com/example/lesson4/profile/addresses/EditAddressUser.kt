package com.example.lesson4.profile.addresses

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.example.lesson4.R
import com.example.lesson4.profile.achivments.UserAddresses
import kotlinx.android.synthetic.main.edit_address_user.*
import kotlinx.android.synthetic.main.edit_address_user.iv_btn_back

class EditAddressUser: Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_address_user)
        iv_btn_back.setOnClickListener { back() }
        initValueInFrom()
        btnSaveChanges.setOnClickListener { validFields() }

    }

    fun back() {
        var intent = Intent(this, UserAddresses::class.java)
        startActivity(intent)
    }
    fun initValueInFrom(){
        var phone = intent.getStringExtra("phone")
        var address = intent.getStringExtra("address")
        var entrance = intent.getStringExtra("entrance")
        var floor = intent.getStringExtra("floor")
        var flat = intent.getStringExtra("flat")
        etPhoneEdit.setText(phone)
        etFullAddressEdit.setText(address)
        etEntranceEdit.setText(entrance)
        etFloorEdit.setText(floor)
        etFlatEdit.setText(flat)
    }

    //валидация  на зполненность полей
    fun validFields(){
        if(etPhoneEdit.text.isEmpty() || etFullAddressEdit.text.isEmpty() ||
            etEntranceEdit.text.isEmpty() || etFloorEdit.text.isEmpty() || etFlatEdit.text.isEmpty())
        {
            Toast.makeText(this, "Заполните все  поля!", Toast.LENGTH_SHORT).show()
        }
        else{
            //код при успешном  заполнении полей
        }
    }
}