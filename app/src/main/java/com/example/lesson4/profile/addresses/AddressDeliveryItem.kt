package com.example.lesson4.profile.addresses

data class AddressDeliveryItem(
    var idAddress: Int,
    var fullAdress:String,
    var entranceNumb:String,
    var floorNumb:String,
    var flatNumb:String,
    var PhoneNumb:String

)
