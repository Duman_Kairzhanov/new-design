package com.example.lesson4.profile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.MenuItem
import android.widget.Toast
import com.example.lesson4.*
import com.example.lesson4.interCity.InterCity
import com.example.lesson4.orders.Orders
import com.example.lesson4.profile.achivments.UserAchivments
import com.example.lesson4.profile.achivments.UserAddresses
import com.example.lesson4.settings.Settings
import com.example.lesson4.support.Support
import com.example.lesson4.terms.TermsOfUse
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.user_profile_main.*

class MainActivity : Activity(), NavigationView.OnNavigationItemSelectedListener {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        nav_view.setNavigationItemSelectedListener(this)

        //кнопка  появления меню
        iv_btn_menu.setOnClickListener {
            drawer_layout.openDrawer(Gravity.LEFT)
        }



        //переход в  адреса доставки
        ly_statistik.setOnClickListener { toAddres() }

        //переход в достижения
        ly_achievements.setOnClickListener { toAchivments() }


        tvCity.setOnClickListener { Toast.makeText(this, "it's work",Toast.LENGTH_SHORT).show() }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        //логика  при клике на  элементы меню
        clickOnItemNavigation(item)
        return true
    }

    fun toAchivments(){
        val intent = Intent(this, UserAchivments::class.java)
        startActivity(intent)
    }
    fun toAddres(){
        val intent = Intent(this, UserAddresses::class.java)
        startActivity(intent)
    }

    fun toSupport(){
        val intent = Intent(this, Support::class.java)
        startActivity(intent)
    }
    fun toProfile(){
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
    fun toTermsOfUse(){
        val intent = Intent(this, TermsOfUse::class.java)
        startActivity(intent)
    }
    fun toSettings(){
        val intent = Intent(this, Settings::class.java)
        startActivity(intent)
    }
    fun toOrders(){
        val intent = Intent(this, Orders::class.java)
        startActivity(intent)
    }
    fun toInterCity() {
        val intent = Intent(this, InterCity::class.java)
        startActivity(intent)
    }

    fun clickOnItemNavigation(item: MenuItem){
        when(item.itemId){
            R.id.nav_support -> { toSupport() }
            R.id.nav_in_city -> {Toast.makeText(this, "City way", Toast.LENGTH_SHORT).show() }
            R.id.nav_between_cities -> {  toInterCity() }
            R.id.nav_delivery -> Toast.makeText(this, "Dostavka", Toast.LENGTH_SHORT).show()
            R.id.nav_driver_mode -> Toast.makeText(this, "Driver mode", Toast.LENGTH_SHORT).show()
            R.id.nav_orders -> { toOrders() }
            R.id.nav_profile ->{ toProfile() }
            R.id.nav_settings -> { toSettings() }
            R.id.nav_use_rules -> { toTermsOfUse() }
            R.id.nav_exit -> Toast.makeText(this, "Quit", Toast.LENGTH_SHORT).show()
        }
    }

}