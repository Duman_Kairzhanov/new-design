package com.example.lesson4.profile.achivments

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.lesson4.R

class AchiveAdapter(listArray: ArrayList<AchiveItem>, context: Context): RecyclerView.Adapter<AchiveAdapter.ViewHolder>() {
    var listItemR = listArray
    val contextR = context
    class ViewHolder(view: View):RecyclerView.ViewHolder(view) {
        var achive_image = view.findViewById<ImageView>(R.id.iv_achive_img)
        var achive_desc = view.findViewById<TextView>(R.id.tv_achive_desc)

        fun bind(listItem: AchiveItem, context: Context) {
            achive_image.setImageResource(listItem.image_id)
            achive_desc.text = listItem.achive_desc
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflator = LayoutInflater.from(contextR)
        return ViewHolder(
            inflator.inflate(
                R.layout.achive_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
       return listItemR.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var listItem = listItemR.get(position)
        holder.bind(listItem,contextR)
    }
}