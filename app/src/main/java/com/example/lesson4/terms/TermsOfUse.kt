package com.example.lesson4.terms

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.MenuItem
import android.widget.Toast
import com.example.lesson4.orders.Orders
import com.example.lesson4.R
import com.example.lesson4.interCity.InterCity
import com.example.lesson4.profile.MainActivity
import com.example.lesson4.settings.Settings
import com.example.lesson4.support.Support
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.terms_of_use_page.*

class TermsOfUse: Activity(), NavigationView.OnNavigationItemSelectedListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.terms_of_use)
        nav_view.setNavigationItemSelectedListener(this)
        iv_btn_menu_tou.setOnClickListener { drawer_layout.openDrawer(Gravity.LEFT) }
        clSetLang.setOnClickListener { toPassengerTermsText() }
        clDriver.setOnClickListener { toDriverTermsText() }
    }

    fun toSupport(){
        val intent = Intent(this, Support::class.java)
        startActivity(intent)
    }

    fun toProfile(){
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    fun toTermsOfUse(){
        val intent = Intent(this, TermsOfUse::class.java)
        startActivity(intent)
    }
    fun toSettings(){
        val intent = Intent(this, Settings::class.java)
        startActivity(intent)
    }
    fun toOrders(){
        val intent = Intent(this, Orders::class.java)
        startActivity(intent)
    }
    fun toInterCity() {
        val intent = Intent(this, InterCity::class.java)
        startActivity(intent)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.nav_support -> { toSupport() }
            R.id.nav_in_city -> {
                Toast.makeText(this, "City way", Toast.LENGTH_SHORT).show() }
            R.id.nav_between_cities -> { toInterCity()  }
            R.id.nav_delivery -> Toast.makeText(this, "Dostavka", Toast.LENGTH_SHORT).show()
            R.id.nav_driver_mode -> Toast.makeText(this, "Driver mode", Toast.LENGTH_SHORT).show()
            R.id.nav_orders -> { toOrders() }
            R.id.nav_profile ->{ toProfile() }
            R.id.nav_settings -> { toSettings() }
            R.id.nav_use_rules -> { toTermsOfUse() }
            R.id.nav_exit -> Toast.makeText(this, "Quit", Toast.LENGTH_SHORT).show()
        }
        return true
    }

    fun toPassengerTermsText(){
        val intent = Intent(this, TermsPassengerPage::class.java)
        startActivity(intent)
    }
    fun toDriverTermsText(){
        val intent = Intent(this, TermsDriverPage::class.java)
        startActivity(intent)
    }

}