package com.example.lesson4.terms

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.example.lesson4.R
import kotlinx.android.synthetic.main.passenger_terms_text.*

class TermsDriverPage: Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.driver_terms_text)

        iv_btn_back.setOnClickListener { back()  }
    }

    fun back(){
        val intent = Intent(this, TermsOfUse::class.java)
        startActivity(intent)
    }
}