package com.example.lesson4.orders.currentOrders

data class CurrentOrderItem (
    var titleInsitution:String? = "",
    var orderNumb:String? = "",
    var institutionImage:Int? = 0,
    var cityImage:Int? = 0,
    var orderdate:String? = "",
    var orderStatus:String? = "",
    var cityFrom:String? = "",
    var cityTo:String? = "",
    var cityDate:String? = "",
    var orderType:String? = ""
)
