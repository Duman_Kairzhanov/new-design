package com.example.lesson4.orders

import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.lesson4.R
import com.example.lesson4.interCity.InterCity
import com.example.lesson4.profile.MainActivity
import com.example.lesson4.settings.Settings
import com.example.lesson4.support.Support
import com.example.lesson4.terms.TermsOfUse
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.orders_page.*


class Orders: AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    val url:String = "https://test.inqar.kz/v3/supply/view?id=589&expand=statuses,supplyProducts,shop&fields=id,created_at,address,status,client_delivery_price,total_price"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.orders)
        nav_view.setNavigationItemSelectedListener(this)
        iv_btn_menu.setOnClickListener {  drawer_layout.openDrawer(Gravity.LEFT) }

        viewpager.adapter =
            CurrentOrdersAndHistoryPageAdapter(
                supportFragmentManager
            )
        tabLayout.setupWithViewPager(viewpager)

        /**auth/
         *
         *
         */


    //end


    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        clickOnItemNavigation(item)
        return true
    }



    fun toSupport(){
        val intent = Intent(this, Support::class.java)
        startActivity(intent)
    }

    fun toProfile(){
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    fun toTermsOfUse(){
        val intent = Intent(this, TermsOfUse::class.java)
        startActivity(intent)
    }

    fun toSettings(){
        val intent = Intent(this, Settings::class.java)
        startActivity(intent)
    }
    fun toOrders(){
        val intent = Intent(this, Orders::class.java)
        startActivity(intent)
    }
    fun toInterCity() {
        val intent = Intent(this, InterCity::class.java)
        startActivity(intent)
    }
    //клик на  элементы меню
    fun clickOnItemNavigation(item: MenuItem){
        when(item.itemId){
            R.id.nav_support -> { toSupport() }
            R.id.nav_in_city -> { Toast.makeText(this, "City way", Toast.LENGTH_SHORT).show() }
            R.id.nav_between_cities -> { toInterCity() }
            R.id.nav_delivery -> Toast.makeText(this, "Dostavka", Toast.LENGTH_SHORT).show()
            R.id.nav_driver_mode -> Toast.makeText(this, "Driver mode", Toast.LENGTH_SHORT).show()
            R.id.nav_orders -> { toOrders() }
            R.id.nav_profile ->{ toProfile() }
            R.id.nav_settings -> { toSettings() }
            R.id.nav_use_rules -> { toTermsOfUse() }
            R.id.nav_exit -> Toast.makeText(this, "Quit", Toast.LENGTH_SHORT).show()
        }
    }
}