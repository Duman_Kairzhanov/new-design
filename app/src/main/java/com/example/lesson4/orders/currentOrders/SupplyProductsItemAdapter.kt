package com.example.lesson4.orders.currentOrders

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.lesson4.R
import com.example.lesson4.model.SupplyProduct

class SupplyProductsItemAdapter(listArray:List<SupplyProduct>):
    RecyclerView.Adapter<SupplyProductsItemAdapter.ViewHolder>() {

    var listArrayR = listArray


    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

        val productName = view.findViewById<TextView?>(R.id.tvProductName)
        val productCount = view.findViewById<TextView?>(R.id.tvQuantity)
        val productPrice = view.findViewById<TextView?>(R.id.tvProductPrice)
        val productImg = view.findViewById<ImageView?>(R.id.ivDriverImage)
        val summaryCount = view.findViewById<ImageView?>(R.id.tvSummaryCount)

        fun bind(listItem: SupplyProduct, context: Context) {
            productName?.text = listItem.productName
            productCount?.text = "x" + listItem.quantity.toString()
            productPrice?.text = listItem.price.toString() + " ₸"

            val radius = context.resources.getDimensionPixelSize(R.dimen.radius)
            Glide.with(context)
                .load(listItem.image)
                .transform(RoundedCorners(radius))
                .into(productImg!!)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(
            inflater.inflate(
                R.layout.supply_products_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return listArrayR.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var listItem = listArrayR.get(position)
        holder.bind(listItem, holder.itemView.context)
    }
}