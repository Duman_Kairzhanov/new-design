package com.example.lesson4.orders.historyOrders

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.lesson4.R
import com.example.lesson4.model.MyOrderInfo
import com.example.lesson4.orders.Orders
import com.example.lesson4.orders.currentOrders.SupplyProductsItemAdapter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.layout_order_history_full_content.*

class ActivityOrderHistoryFullContentItem: AppCompatActivity() {

    val url:String = "https://test.inqar.kz/v3/supply/view?id=781&expand=statuses,supplyProducts,shop&fields=id,created_at,address,status,client_delivery_price,total_price"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_order_history_full_content)
        ivDriverImage.setImageResource(intent.getIntExtra("orderImg",
            R.drawable.zirra
        ))
        tvInstitutionName.text = intent.getStringExtra("title")
        tvOrderNumb.text = intent.getStringExtra("orderNumb")
        tvOrderDate.text = intent.getStringExtra("orderDate")
        tvOrderStatus.text = intent.getStringExtra("orderStatus")
        iv_btn_back.setOnClickListener {  back() }
        tvTimeStartOrder.text = intent.getStringExtra("orderDate")

        //api
        var request: StringRequest? = object :
            StringRequest(Request.Method.GET, url, object : Response.Listener<String?> {
                override fun onResponse(response: String?) {
                    if (response != null) {


                        val gson = Gson()
                        val itemType = object : TypeToken<MyOrderInfo>() {}.type
                        val shop = gson.fromJson<MyOrderInfo>(response, itemType)
                        val list = shop.data

                        rcViewSupplyProducts.adapter =
                            SupplyProductsItemAdapter(
                                list.supplyProducts
                            )
                        rcViewSupplyProducts.hasFixedSize()
                        rcViewSupplyProducts.layoutManager = LinearLayoutManager(this@ActivityOrderHistoryFullContentItem)
                        tvSummaryCount.text = "${list.totalPrice} ${getString(R.string.tenge)}"
                        tvPriceOfDelivery.text = "${list.clientDeliveryPrice} ${getString(
                            R.string.tenge
                        )}"

                        //проверка  - есть ли  дата в состоянии доставки и присвоение значений в  поле "курьер"
                        if(list.statuses.inWay.isEmpty()){
                            tvTimeCourierInAway.visibility = View.GONE
                            tvTimeCourierInAway.gravity = Gravity.CENTER_VERTICAL
                        }else{
                            tvTimeCourierInAway.text = list.statuses.inWay
                        }
                        //проверка  - есть ли  дата в состоянии доставки и присвоение значений в  поле "доставка"
                        if(list.statuses.delivered.isEmpty()){
                            tvTimeDelivered.visibility = View.GONE
                            tvTimeDelivered.gravity = Gravity.CENTER_VERTICAL
                        }else{
                            tvTimeDelivered.text = list.statuses.delivered
                        }

                        if(list.status == 7){
                            //если все  доставлено
                            Toast.makeText(this@ActivityOrderHistoryFullContentItem, "доставлено все", Toast.LENGTH_SHORT).show()
                            //таймлайны, если все доставлено
                            vDeliveryUnder.setBackgroundColor(getResources().getColor(R.color.colorGreenSuccessLine) )
                            vCurierUp.setBackgroundColor(getResources().getColor(R.color.colorGreenSuccessLine) )
                            vCurierUnder.setBackgroundColor(getResources().getColor(R.color.colorGreenSuccessLine) )
                            vDelivUp.setBackgroundColor(getResources().getColor(R.color.colorGreenSuccessLine) )
                            tvTimeCourierInAway.visibility = View.VISIBLE
                        }else if(list.status == 4){
                            //если в процессе доставки(в состоянии курьер)
                            Toast.makeText(this@ActivityOrderHistoryFullContentItem, "в процессе, чувак", Toast.LENGTH_SHORT).show()
                            //появление тайм линий
                            vDeliveryUnder.setBackgroundColor(getResources().getColor(R.color.colorGreenSuccessLine) )
                            vCurierUp.setBackgroundColor(getResources().getColor(R.color.colorGreenSuccessLine) )
                            //делаем  серым чекбокс
                            ivThirdCheck.setImageResource(R.drawable.ic_unclicked_false_status)
                            //стилизуем 3 состояние доставки  - в неактивное
                            imThirdState.setImageResource(R.drawable.ic_delivered_status) // серенький
                            tvDeliveredText.setTextColor(getResources().getColor(R.color.colorGrey))

                        }
                        else if(list.status == 8) {
                            //если заказ отменен клиентом
                            //сменяем картинки, когда заказ ОТМЕНЕН: в курьере и доставке(2,3 состояние их 3 в общем)
                            ivFirstState.setImageResource(R.drawable.ic_canceled_order) //красный крест, типа отмена
                            imThirdState.setImageResource(R.drawable.ic_delivered_status) // серенький
                            ivSecondState.setImageResource(R.drawable.ic_courier_on_the_way) // серенький

                            //смнена чекбоксов на серый
                            ivSecondCheck.setImageResource(R.drawable.ic_unclicked_false_status)
                            ivThirdCheck.setImageResource(R.drawable.ic_unclicked_false_status)

                            //мена цвета надписей
                            tvFirstStateText.setTextColor(getResources().getColor(R.color.colorRedError))
                            tvFirstStateText.text = "Заказ отменен"
                            tvCourierText.setTextColor(getResources().getColor(R.color.colorGrey))
                            tvDeliveredText.setTextColor(getResources().getColor(R.color.colorGrey))
                            //убираем дату у  курьера, ведь заказ  отменен
                            tvTimeCourierInAway.visibility = View.GONE
                        }


                    } else {
                        Log.e("Your Array Response", "Data Null")
                        Toast.makeText(applicationContext, "Error you", Toast.LENGTH_SHORT).show()
                    }
                }
            }, object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {
                    Log.e("error is ", "" + error)
                }
            }) {
            //This is for Headers If You Needed
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val params: MutableMap<String, String> =
                    HashMap()
                params["Content-Type"] = "application/json; charset=UTF-8"
                params["Authorization"] = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9leGFtcGxlLm9yZyIsInVpZCI6MTU2fQ.r9mqVqCX6lrRPoZrP6uWusAlfV_xS3bFWIYnrz4Lq-Q"
                return params
            }

        }
        val queue = Volley.newRequestQueue(applicationContext)
        queue.add(request)
        //api


    }

    fun back() {
        val intent = Intent(this, Orders::class.java)
        startActivity(intent)
    }
}