package com.example.lesson4.orders.historyOrders

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.lesson4.R
import com.example.lesson4.orders.currentOrders.CurrentOrderItem
import kotlinx.android.synthetic.main.fragment_history.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class HistoryFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    var listOrders = ArrayList<CurrentOrderItem>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_history, container, false)
    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            HistoryFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    fun addOrdersAndTaxi() {
        listOrders.add(
            CurrentOrderItem(
                titleInsitution = "Zira",
                orderNumb = "#0001",
                institutionImage = R.drawable.zirra,
                orderdate = "12 июля 18:57",
                orderStatus = "Доставлен",
                orderType = "delivery"
            )
        )
        listOrders.add(
            CurrentOrderItem(
                cityImage = R.drawable.ic_status_taxi_city,
                cityFrom = "Ермекова 85",
                cityTo = "Ерубаева 34",
                cityDate = " 28 ентября 18:36",
                orderType = "taxi"
            )
        )
        listOrders.add(
            CurrentOrderItem(
                cityImage = R.drawable.ic_status_taxi_city,
                cityFrom = "пр. Жамбыла 46 аптека Здоровье",
                cityTo = "Кривогуза, 10",
                cityDate = "5 июля 14:24",
                orderType = "taxi"
            )
        )
        listOrders.add(
            CurrentOrderItem(
                titleInsitution = "Korea Town",
                orderNumb = "#125211",
                institutionImage = R.drawable.korea_town,
                orderdate = "28 июня 12:43",
                orderStatus = "Доставлен",
                orderType = "delivery"
            )
        )
        listOrders.add(
            CurrentOrderItem(
                titleInsitution = "La Villa",
                orderNumb = "#125201",
                institutionImage = R.drawable.la_villa,
                orderdate = "28 июня 12:43",
                orderStatus = "Доставлен",
                orderType = "delivery"
            )
        )
        listOrders.add(
            CurrentOrderItem(
                titleInsitution = "Zira",
                orderNumb = "#0005",
                institutionImage = R.drawable.zirra,
                orderdate = "24 августа 17:10",
                orderStatus = "Доставлен",
                orderType = "delivery"
            )
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addOrdersAndTaxi()
        activity?.let {
            rcViewHistory.adapter =
                HistoryOrdersAdapter(
                    listOrders,
                    it
                )
            rcViewHistory.hasFixedSize()
            rcViewHistory.layoutManager = LinearLayoutManager(it)
        }

    }
}