package com.example.lesson4.orders.currentOrders

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.lesson4.R

class CurrentOrdersAdapter(listArray:ArrayList<CurrentOrderItem>, context: Context):
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var listArrayR = listArray
    var contextR = context

    inner class CurrentOrdersViewHolder(view: View): RecyclerView.ViewHolder(view){
        val titleInstitution:TextView =view.findViewById(R.id.tvInstitutionName)
        val OrderNumb:TextView =view.findViewById(R.id.tvOrderNumb)
        val InstitutionImage:ImageView =view.findViewById(R.id.ivDriverImage)
        val OrderDate:TextView =view.findViewById(R.id.tvOrderDate)
        val OrderStatus:TextView =view.findViewById(R.id.tvOrderStatus)

    }

    inner class HistoryViewHolder(view: View): RecyclerView.ViewHolder(view){
        val taxiImage: ImageView =view.findViewById(R.id.imageView)
        val addrFrom:TextView =view.findViewById(R.id.tvAddrFrom)
        val addrTo:TextView =view.findViewById(R.id.tvAddrTo)
        val taxiDate:TextView =view.findViewById(R.id.tvDateTimeTaxi)
    }

    companion object {
        const val TYPE_TAXI = 0
        const val TYPE_ORDER = 1
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {

        return when (viewType) {
            TYPE_ORDER -> CurrentOrdersViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.current_orders_item, parent, false)
            )
            TYPE_TAXI -> HistoryViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.history_orders_city_item, parent, false)
            )
            else -> throw IllegalArgumentException()
        }
    }

    override fun getItemCount(): Int {
        return listArrayR.size
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        var listItem = listArrayR.get(position)
        when (holder.itemViewType){
            TYPE_TAXI -> onBindTaxi(holder, listItem)
            TYPE_ORDER -> onBindDelivery(holder, listItem)
            else -> throw IllegalArgumentException()
        }
    }

    fun onBindDelivery(holder: RecyclerView.ViewHolder, rowDate: CurrentOrderItem){
        val dateHolder = holder as CurrentOrdersViewHolder
        dateHolder.InstitutionImage!!.setImageResource(rowDate.institutionImage!!)
        dateHolder.titleInstitution.text = rowDate.titleInsitution
        dateHolder.OrderNumb.text = rowDate.orderNumb
        dateHolder.OrderDate.text = rowDate.orderdate
        dateHolder.OrderStatus.text = rowDate.orderStatus
        dateHolder.itemView.setOnClickListener {
            Toast.makeText(contextR, "${rowDate.titleInsitution}", Toast.LENGTH_SHORT).show()
            var i = Intent(contextR, ActivityOrderCurrentFullContentItem::class.java).apply{
                putExtra("title", rowDate.titleInsitution)
                putExtra("orderNumb", rowDate.orderNumb)
                putExtra("orderDate", rowDate.orderdate)
                putExtra("orderStatus", rowDate.orderStatus)
                putExtra("orderImg", rowDate.institutionImage!!)
            }
            contextR.startActivity(i)
        }
    }
    fun onBindTaxi(holder: RecyclerView.ViewHolder, rowDate: CurrentOrderItem){
        val dateHolder = holder as HistoryViewHolder
        dateHolder.taxiImage.setImageResource(rowDate.cityImage!!)
        dateHolder.addrFrom.text = rowDate.cityFrom
        dateHolder.addrTo.text = rowDate.cityTo
        dateHolder.taxiDate.text = rowDate.cityDate
        dateHolder.itemView.setOnClickListener {
            Toast.makeText(contextR, "${rowDate.cityDate}", Toast.LENGTH_SHORT).show()
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (listArrayR[position].orderType == "taxi" ) TYPE_TAXI else TYPE_ORDER
    }

}