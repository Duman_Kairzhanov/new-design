package com.example.lesson4.orders.historyOrders

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.lesson4.R
import com.example.lesson4.orders.Orders
import kotlinx.android.synthetic.main.layout_taxi_history_full_content.*

class ActivityTaxiHistoryFullContentItem: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_taxi_history_full_content)
        iv_btn_back.setOnClickListener {  back() }
        tvTimeTaxi.text = intent.getStringExtra("taxiDate")
        tvAddressFrom.text = intent.getStringExtra("addressFrom")
        tvAddressTo.text = intent.getStringExtra("addressTo")

    }

    fun back() {
        val intent = Intent(this, Orders::class.java)
        startActivity(intent)
    }
}