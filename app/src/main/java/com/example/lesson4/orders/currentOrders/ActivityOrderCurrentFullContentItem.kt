package com.example.lesson4.orders.currentOrders

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.lesson4.R
import com.example.lesson4.model.MyOrderInfo
import com.example.lesson4.orders.Orders
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.order_full_content_item.*
import kotlinx.android.synthetic.main.order_full_content_item.ivDriverImage
import kotlinx.android.synthetic.main.order_full_content_item.ivThirdCheck
import kotlinx.android.synthetic.main.order_full_content_item.iv_btn_back
import kotlinx.android.synthetic.main.order_full_content_item.rcViewSupplyProducts
import kotlinx.android.synthetic.main.order_full_content_item.tvDeliveredText
import kotlinx.android.synthetic.main.order_full_content_item.tvInstitutionName
import kotlinx.android.synthetic.main.order_full_content_item.tvOrderDate
import kotlinx.android.synthetic.main.order_full_content_item.tvOrderNumb
import kotlinx.android.synthetic.main.order_full_content_item.tvOrderStatus
import kotlinx.android.synthetic.main.order_full_content_item.tvPriceOfDelivery
import kotlinx.android.synthetic.main.order_full_content_item.tvSummaryCount
import kotlinx.android.synthetic.main.order_full_content_item.tvTimeStartOrder
import kotlinx.android.synthetic.main.order_full_content_item.vCurierUp
import kotlinx.android.synthetic.main.order_full_content_item.vDeliveryUnder

class ActivityOrderCurrentFullContentItem: AppCompatActivity() {

    val url:String = "https://test.inqar.kz/v3/supply/view?id=780&expand=statuses,supplyProducts,shop&fields=id,created_at,address,status,client_delivery_price,total_price"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.order_full_content_item)
        ivDriverImage.setImageResource(intent.getIntExtra("orderImg",
            R.drawable.zirra
        ))
        tvInstitutionName.text = intent.getStringExtra("title")
        tvOrderNumb.text = intent.getStringExtra("orderNumb")
        tvOrderDate.text = intent.getStringExtra("orderDate")
        tvOrderStatus.text = intent.getStringExtra("orderStatus")
        iv_btn_back.setOnClickListener {  back() }
        tvTimeStartOrder.text = intent.getStringExtra("orderDate")

        //api
        var request: StringRequest? = object :
            StringRequest(Request.Method.GET, url, object : Response.Listener<String?> {
                override fun onResponse(response: String?) {
                    if (response != null) {


                        val gson = Gson()
                        val itemType = object : TypeToken<MyOrderInfo>() {}.type
                        val shop = gson.fromJson<MyOrderInfo>(response, itemType)
                        val list = shop.data

                        rcViewSupplyProducts.adapter =
                            SupplyProductsItemAdapter(
                                list.supplyProducts
                            )
                        rcViewSupplyProducts.hasFixedSize()
                        rcViewSupplyProducts.layoutManager = LinearLayoutManager(this@ActivityOrderCurrentFullContentItem)
                        tvSummaryCount.text = "${list.totalPrice} ${getString(R.string.tenge)}"
                        tvPriceOfDelivery.text = "${list.clientDeliveryPrice} ${getString(
                            R.string.tenge
                        )}"

                        if(list.status == 4){
                            //если в процессе доставки(в состоянии курьер)
                            Toast.makeText(this@ActivityOrderCurrentFullContentItem, "в процессе, чувак", Toast.LENGTH_SHORT).show()
                            //появление тайм линий
                            vDeliveryUnder.setBackgroundColor(getResources().getColor(R.color.colorGreenSuccessLine) )
                            vCurierUp.setBackgroundColor(getResources().getColor(R.color.colorGreenSuccessLine) )
                            //делаем  серым чекбокс
                            ivThirdCheck.setImageResource(R.drawable.ic_unclicked_false_status)
                            //стилизуем 3 состояние доставки  - в неактивное
                            imThirdState.setImageResource(R.drawable.ic_delivered_status) // серенький
                            tvDeliveredText.setTextColor(getResources().getColor(R.color.colorGrey)) //цвет шрифта
                            //стилизуем в  активное чексбокс курьера
                            ivSecondCheck.setImageResource(R.drawable.ic_clicked_true_check)
                            // меняем картинку у  курьера на  активную
                            ivSecondState.setImageResource(R.drawable.ic_curier_success)
                            tvCourierText.setTextColor(getResources().getColor(R.color.colorOrangeSuccessCourier))
                            tvTimeCourierInAway.text = list.statuses.inWay

                        }

                    } else {
                        Log.e("Your Array Response", "Data Null")
                        Toast.makeText(applicationContext, "Error you", Toast.LENGTH_SHORT).show()
                    }
                }
            }, object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {
                    Log.e("error is ", "" + error)
                }
            }) {
            //This is for Headers If You Needed
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val params: MutableMap<String, String> =
                    HashMap()
                params["Content-Type"] = "application/json; charset=UTF-8"
                params["Authorization"] = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9leGFtcGxlLm9yZyIsInVpZCI6MTU2fQ.r9mqVqCX6lrRPoZrP6uWusAlfV_xS3bFWIYnrz4Lq-Q"
                return params
            }

        }
        val queue = Volley.newRequestQueue(applicationContext)
        queue.add(request)
        //api
    }

    fun back() {
        val intent = Intent(this, Orders::class.java)
        startActivity(intent)
    }
}