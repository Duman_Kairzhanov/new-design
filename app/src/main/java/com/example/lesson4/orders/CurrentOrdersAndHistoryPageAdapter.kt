package com.example.lesson4.orders

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.lesson4.orders.currentOrders.CurrentOrdersFragment
import com.example.lesson4.orders.historyOrders.HistoryFragment

class CurrentOrdersAndHistoryPageAdapter(fm:FragmentManager):FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        when(position){
            0 -> { return CurrentOrdersFragment()
            }
            1 -> { return HistoryFragment()
            }
            else -> { return CurrentOrdersFragment()
            }
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when(position){
            0 -> { return "Текущие заказы" }
            1 -> { return "История" }
        }
        return super.getPageTitle(position)
    }

    override fun getCount(): Int {
        return 2
    }

}