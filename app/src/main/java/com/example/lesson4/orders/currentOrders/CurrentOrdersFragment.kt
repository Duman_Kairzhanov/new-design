package com.example.lesson4.orders.currentOrders

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.lesson4.R
import kotlinx.android.synthetic.main.fragment_current_orders.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [CurrentOrdersFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class CurrentOrdersFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    var listOrders = ArrayList<CurrentOrderItem>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_current_orders, container, false)
    }

    fun addOrdersAndTaxi() {
        listOrders.add(
            CurrentOrderItem(
                titleInsitution = "Zira",
                orderNumb = "#0001",
                institutionImage = R.drawable.zirra,
                orderdate = "12 июля 18:57",
                orderStatus = "Закаказ готовят",
                orderType = "delivery"
            )
        )
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment CurrentOrdersFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            CurrentOrdersFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addOrdersAndTaxi()
        activity?.let {
            rcViewCurrentOrders.adapter =
                CurrentOrdersAdapter(
                    listOrders,
                    it
                )
            rcViewCurrentOrders.hasFixedSize()
            rcViewCurrentOrders.layoutManager = LinearLayoutManager(it)
        }

    }
}