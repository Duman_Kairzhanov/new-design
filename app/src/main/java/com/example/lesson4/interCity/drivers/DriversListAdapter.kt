package com.example.lesson4.interCity.drivers

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.lesson4.R
import com.example.lesson4.model.drivers.Data


class DriversListAdapter(listArray: List<com.example.lesson4.model.drivers.Data>, context: Context):
    RecyclerView.Adapter<DriversListAdapter.ViewHolder>() {

    var listItemR = listArray
    val contextR = context
    var onItemClick: ((Data) -> Unit)? = null

    inner class ViewHolder(view: View):RecyclerView.ViewHolder(view){
        var imageDriver = view.findViewById<ImageView>(R.id.ivDriverImage)
        var nameDriver = view.findViewById<TextView>(R.id.tvNameOfDriver)
        var ratingDriver = view.findViewById<TextView>(R.id.tvRatingOfDriver)
        var priceOfTaxi = view.findViewById<TextView>(R.id.tvPriceOfDriver)
        var dateOfTrip = view.findViewById<TextView>(R.id.tvDateOfTrip)
        var addressFrom = view.findViewById<TextView>(R.id.tvAddressFromCity)
        var addressTo = view.findViewById<TextView>(R.id.tvAddressToCity)
        var someCommentDriver = view.findViewById<TextView>(R.id.tvSomeComment)
        var phoneDriver = view.findViewById<TextView>(R.id.tvPhoneDriver)

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(listItemR[adapterPosition])
            }
        }

        fun bind(listItem: Data, context: Context) {
            val radius = context.resources.getDimensionPixelSize(R.dimen.radiusDriver)
            Glide.with(context)
                .load(listItem.creatorInfo.avatar)
                .transform(RoundedCorners(radius))
                .into(imageDriver)
            nameDriver.text = listItem.creatorInfo.fio
            ratingDriver.text = listItem.creatorInfo.driverRating
            priceOfTaxi.text = listItem.price
            dateOfTrip.text = listItem.dateStart
            addressFrom.text = listItem.locationFrom
            addressTo.text = listItem.locationTo
            someCommentDriver.text = listItem.comment
            phoneDriver.text = listItem.creatorInfo.phone

        }


    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
       var inflator = LayoutInflater.from(parent.context)
        return ViewHolder(
            inflator.inflate(R.layout.drivers_list_item, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return listItemR.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var listItem = listItemR.get(position)
        return holder.bind(listItem, holder.itemView.context)
    }

}