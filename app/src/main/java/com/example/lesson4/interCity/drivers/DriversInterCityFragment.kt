package com.example.lesson4.interCity.drivers

import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.lesson4.R
import com.example.lesson4.model.drivers.DriversList
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.dialog_agred_with_driver.*
import kotlinx.android.synthetic.main.fragment_drivers_inter_city.*
import org.json.JSONObject
import java.util.*


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
var flagCall: Boolean? = false
var sendInfo: Boolean = false
private val url: String = "https://test.inqar.kz/v3/ride/drivers-list"
private var lasPhoneClicked: String? = ""

var volleyRequestQueue: RequestQueue? = null
private val urlPost: String = "https://test.inqar.kz/v3/ride/add-history"


class DriversInterCityFragment : Fragment() {

    var listDrivers = ArrayList<DriversListItem>()

    fun addDriver() {
        listDrivers.add(
            DriversListItem(
                R.drawable.tolerant,
                "Anaconda",
                "5.0",
                1800,
                "10 окт., 16:30",
                "Караганда",
                "Шахтинск",
                "Не забудь плетку",
                "87026692355"
            )
        )
        listDrivers.add(
            DriversListItem(
                R.drawable.person,
                "Серега",
                "3.4",
                4000,
                "23 окт., 14:10",
                "Нур-Султан",
                "Экибастуз",
                "Забыли сумку с грибами",
                "87051236699"
            )
        )
        listDrivers.add(
            DriversListItem(
                R.drawable.black_man,
                "Онолулу",
                "4.3",
                2500,
                "23 окт., 14:10",
                "Караганда",
                "Темиртау",
                "Возьми обезбаливающее",
                "87014478569"
            )
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        loadData()
        flagCall = false
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_drivers_inter_city, container, false)
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            DriversInterCityFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    fun startCall(tel: String) {
        val callIntent = Intent(Intent.ACTION_DIAL)
        callIntent.data = Uri.parse("tel:" + tel)
        startActivity(callIntent)
    }

    //my code
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        //вызов  функциидобавление элементов в   ресайклесвью
        addDriver()

        //api start
        var request: StringRequest? = object :
            StringRequest(Request.Method.GET, url, object : Response.Listener<String?> {
                override fun onResponse(response: String?) {
                    if (response != null) {


                        val gson = Gson()
                        val itemType = object : TypeToken<DriversList>() {}.type
                        val drivers = gson.fromJson<DriversList>(response, itemType)
                        val list = drivers.data

                        val adapter = DriversListAdapter(list, activity!!)
                        rcViewDrivers.adapter = adapter
                        rcViewDrivers.hasFixedSize()
                        rcViewDrivers.layoutManager = LinearLayoutManager(activity!!)

                        adapter.onItemClick = {
                            startCall(it.creatorInfo.phone)
                            Toast.makeText(context, "It's work!", Toast.LENGTH_SHORT).show()
                            flagCall = true
                            saveFlag()
                            savePhoneWhenClicked(it.creatorInfo.phone)

                        }


                    } else {
                        Log.e("Your Array Response", "Data Null")
                        Toast.makeText(activity!!, "Error you", Toast.LENGTH_SHORT).show()
                    }
                }
            }, object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {
                    Log.e("error is ", "" + error)
                }
            }) {
            //This is for Headers If You Needed
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val params: MutableMap<String, String> =
                    HashMap()
                params["Content-Type"] = "application/json; charset=UTF-8"
                params["Authorization"] =
                    "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9leGFtcGxlLm9yZyIsInVpZCI6MTU2fQ.r9mqVqCX6lrRPoZrP6uWusAlfV_xS3bFWIYnrz4Lq-Q"
                return params
            }

        }
        val queue = Volley.newRequestQueue(activity!!)
        queue.add(request)
        //api end


        //обработка  события при клике  на  календарик
        imCalendarBtn.setOnClickListener {
            val dpd = DatePickerDialog(
                activity!!,
                DatePickerDialog.OnDateSetListener { view: DatePicker, mYear: Int, mMonth: Int, mDay: Int ->
                    tvDateTime.text = "${mDay}.${mMonth}.${mYear}"

                },
                year,
                month,
                day
            )
            dpd.show()
            dpd.getDatePicker().setMinDate(calendar.getTimeInMillis())
            tvDateTime.visibility = View.VISIBLE
        }

        //обработка клика на какой-нибудь элемент recyclerView
//        adapter.onItemClick = {
//            startCall(it.phoneDriver)
//            Toast.makeText(context, "It's work!", Toast.LENGTH_SHORT).show()
//            flagCall = true
//            saveFlag()
//          if(sendInfo){
//              var bundle = Bundle()
//              bundle.putInt("imgDriver", it.imageDriver)
//              bundle.putString("nameDriver", it.nameDriver)
//              bundle.putString("ratingDriver", it.ratingDriver)
//              bundle.putString("ratingDriver", it.ratingDriver)
//              bundle.putInt("priceDriver", it.priceOfDriver)
//              bundle.putString("dateOfTrip", it.dateOfTaxi)
//              bundle.putString("addrFrom", it.addressFrom)
//              bundle.putString("addrTo", it.addressTo)
//              bundle.putString("someComment", it.someCommentFromDriver)
//              var fragment = HistoryInterCityFragment()
//              fragment.setArguments(bundle)
//          }
//        }
        /*sdgshg*/


    }


    //1 post api
    private fun post(phoneNumb: String) {

        //параметры которые ты отправляешь
        val parameters: MutableMap<String, String> =
            HashMap()
        parameters["phone"] = phoneNumb

        val request: JsonObjectRequest = object :
            JsonObjectRequest(
                Request.Method.POST,
                urlPost,
                JSONObject(parameters as Map<*, *>),
                object : Response.Listener<JSONObject> {
                    override fun onResponse(response: JSONObject) {
                        if (response != null) {
                            Toast.makeText(activity!!, "shittt", Toast.LENGTH_SHORT).show()
                        } else {
                            Toast.makeText(activity!!, "faggot", Toast.LENGTH_SHORT).show()
                        }
                    }
                },
                object : Response.ErrorListener {
                    override fun onErrorResponse(error: VolleyError) {
                        Log.e("error is ", "" + error)
                        Toast.makeText(activity!!, error.toString(), Toast.LENGTH_SHORT).show()
                        VolleyLog.v("error is ", "" + error)
                    }
                }) {
            /*override fun getParams(): MutableMap<String, String> {
                return parameters
            }*/

            //This is for Headers If You Needed
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val params: MutableMap<String, String> = HashMap()
                params["Content-Type"] = "application/json; charset=UTF-8"
                params["Authorization"] =
                    "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9leGFtcGxlLm9yZyIsInVpZCI6MTU2fQ.r9mqVqCX6lrRPoZrP6uWusAlfV_xS3bFWIYnrz4Lq-Q"
                return params
            }
        }
        val queue = Volley.newRequestQueue(activity!!)
        VolleyLog.DEBUG = true;
        queue.add(request)
    }
    //2

    // Adding request to request queue


    fun saveFlag() {
        val sharedPreferences =
            this.getActivity()?.getSharedPreferences("flagCall", Context.MODE_PRIVATE)
        val editor = sharedPreferences?.edit()
        editor.apply {
            this!!.putBoolean("BOOLEAN_KEY", flagCall!!)
        }!!.apply()
    }

    fun loadData() {
        val sharedPreferences =
            this.getActivity()?.getSharedPreferences("flagCall", Context.MODE_PRIVATE)
        var savedFlag = sharedPreferences?.getBoolean("BOOLEAN_KEY", false)
        flagCall = savedFlag
    }

    fun savePhoneWhenClicked(phoneNumb: String) {
        val sharedPreferences =
            this.getActivity()?.getSharedPreferences("savePhone", Context.MODE_PRIVATE)
        val editor = sharedPreferences?.edit()
        editor.apply {
            this!!.putString("STRING_KEY", phoneNumb)
        }!!.apply()
    }

    fun loadPhone() {
        val sharedPreferences =
            this.getActivity()?.getSharedPreferences("savePhone", Context.MODE_PRIVATE)
        var savedPhone = sharedPreferences?.getString("STRING_KEY", "")
        lasPhoneClicked = savedPhone
    }

    override fun onResume() {
        super.onResume()
        if (flagCall!!) {

            val setLangDialog = BottomSheetDialog(activity!!, R.style.SheetDialog)
            setLangDialog.window?.attributes?.windowAnimations =
                R.style.DialogAnimation_2 //style id
            setLangDialog.window?.attributes?.windowAnimations
            setLangDialog.setContentView(R.layout.dialog_agred_with_driver)
            setLangDialog.show()
            setLangDialog.tvNoAgreed.setOnClickListener {
                setLangDialog.dismiss()
            }
            setLangDialog.tvYesAgreed.setOnClickListener {
                loadPhone()
                Toast.makeText(activity!!, "${lasPhoneClicked}", Toast.LENGTH_SHORT).show()
                sendInfo = true
                setLangDialog.dismiss()
                post(lasPhoneClicked!!)
                Toast.makeText(
                    activity!!,
                    "Отправлено вот это: ${lasPhoneClicked}",
                    Toast.LENGTH_SHORT
                ).show()

            }
            flagCall = false
        }
    }

    // post query send phone of Driver in history
    //start


    // end
}