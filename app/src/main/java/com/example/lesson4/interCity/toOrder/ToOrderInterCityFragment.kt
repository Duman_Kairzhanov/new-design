package com.example.lesson4.interCity.toOrder

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.lesson4.R
import com.example.lesson4.model.toOrder.GetCurrentOrder
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_to_order.*
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ToOrderFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ToOrderFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var statusCode: Int? = 0
    private var currentOrderId: Int? = 0
    private val urlPostCancelOrder = "https://test.inqar.kz/v3/ride/cancel"
    private val urlCreateOrder = "https://test.inqar.kz/v3/ride/passenger-create"
    private var responseStatus: Int? = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_to_order, container, false)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ToOrderFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ToOrderFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getCurrentOrderFromApi()

        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        activity?.let {
            val context = it
            imCalendarBtn.setOnClickListener {
                val dpd = DatePickerDialog(
                    context,
                    DatePickerDialog.OnDateSetListener { view: DatePicker, mYear: Int, mMonth: Int, mDay: Int ->
                        Thread {
                            Thread.sleep(800)
                            activity!!.runOnUiThread {
                                tvDate.visibility = View.VISIBLE
                            }
                        }.start()

                        tvDate.text = "${mDay}.${mMonth}.${mYear}"

                    },
                    year,
                    month,
                    day
                )
                dpd.show()
                dpd.getDatePicker().setMinDate(calendar.getTimeInMillis())
                tvDate.visibility = View.VISIBLE
                val cal = Calendar.getInstance()
                val timeSetListener =
                    TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
                        cal.set(Calendar.HOUR_OF_DAY, hour)
                        cal.set(Calendar.MINUTE, minute)
                        tvTime.text = SimpleDateFormat("HH:mm").format(cal.time)

                    }
                val style = R.style.SpinnerTimePickerDialog

                TimePickerDialog(
                    activity!!,
                    style,
                    timeSetListener,
                    cal.get(Calendar.HOUR_OF_DAY),
                    cal.get(Calendar.MINUTE),
                    true
                ).show()
                tvTime.visibility = View.VISIBLE
            }
            btnToOrderTaxi.setOnClickListener {
                if (etAddressFrom.text.isEmpty() || etAddressTo.text.isEmpty() || etPriceTaxi.text.isEmpty()) {
                    Toast.makeText(context, "Заполните все поля", Toast.LENGTH_SHORT).show()
                } else {
                    //код, если все поля введены
                    hideFirstBlocksWhenClicked()
                    createNewOrder()
                }
            }
            fabCancel.setOnClickListener {
                fabCancel.getResources().getColor(R.color.colorRedError)
                hideSecondBlockWhenXClicked()
                cancelCurrentOrder(currentOrderId!!)
            }


        }

    }

    private fun hideFirstBlocksWhenClicked() {
        //скрывает первый экран
        etAddressFrom.visibility = View.GONE
        etAddressTo.visibility = View.GONE
        imCalendarBtn.visibility = View.GONE
        tvDate.visibility = View.GONE
        etPriceTaxi.visibility = View.GONE
        etComment.visibility = View.GONE
        view4.visibility = View.GONE
        view7.visibility = View.GONE
        view10.visibility = View.GONE
        btnToOrderTaxi.visibility = View.GONE

        //передаем  данные  второму  экрану
        tvFromAdrOrder.text = etAddressFrom.text
        tvToAdrOrder.text = etAddressTo.text
        tvDateOrder.text = tvDate.text
        tvPriceOrder.text = etPriceTaxi.text
        tvCommentOrder.text = etComment.text

        //появляетчя второй  экран
        tvFromAdrOrder.visibility = View.VISIBLE
        tvToAdrOrder.visibility = View.VISIBLE
        tvPriceOrder.visibility = View.VISIBLE
        tvDateOrder.visibility = View.VISIBLE
        commentBlock.visibility = View.VISIBLE
        tvCommentOrder.visibility = View.VISIBLE
        fabCancel.visibility = View.VISIBLE
        fabOk.visibility = View.VISIBLE
        ivTaxiImg.visibility = View.VISIBLE
    }

    private fun hideSecondBlockWhenXClicked() {
        //скрывается второй  экран
        tvFromAdrOrder.visibility = View.GONE
        tvToAdrOrder.visibility = View.GONE
        tvPriceOrder.visibility = View.GONE
        tvDateOrder.visibility = View.GONE
        tvCommentOrder.visibility = View.GONE
        tvCommentOrder.visibility = View.GONE
        fabCancel.visibility = View.GONE
        fabOk.visibility = View.GONE

        // появляется  первый  экран
        etAddressFrom.visibility = View.VISIBLE
        etAddressTo.visibility = View.VISIBLE
        imCalendarBtn.visibility = View.VISIBLE
        tvDate.visibility = View.VISIBLE
        etPriceTaxi.visibility = View.VISIBLE
        etComment.visibility = View.VISIBLE
        view4.visibility = View.VISIBLE
        view7.visibility = View.VISIBLE
        view10.visibility = View.VISIBLE
        btnToOrderTaxi.visibility = View.VISIBLE
        ivTaxiImg.visibility = View.VISIBLE

    }

    private fun getCurrentOrderFromApi() {
        val url = "https://test.inqar.kz/v3/ride/passenger-active"
        //api start
        var request: StringRequest? = object :
            StringRequest(Request.Method.GET, url, object : Response.Listener<String?> {

                override fun onResponse(response: String?) {

                    if (response != null) {
                        progressBar.visibility = View.GONE
                        Toast.makeText(activity!!, "${statusCode}", Toast.LENGTH_SHORT).show()
                        hideFirstBlocksWhenClicked()
                        val gson = Gson()
                        val itemType = object : TypeToken<GetCurrentOrder>() {}.type
                        val drivers = gson.fromJson<GetCurrentOrder>(response, itemType)
                        val list = drivers.data
                        currentOrderId = list.id
                        tvFromAdrOrder.text = list.locationFrom
                        tvToAdrOrder.text = list.locationTo
                        tvPriceOrder.text = list.price
                        tvDateOrder.text = list.dateStart
                        tvCommentOrder.text = list.comment
//                        val adapter = DriversListAdapter(list, activity!!)
//                        rcViewDrivers.adapter = adapter
//                        rcViewDrivers.hasFixedSize()
//                        rcViewDrivers.layoutManager = LinearLayoutManager(activity!!)

//                        adapter.onItemClick = {
//                            startCall(it.creatorInfo.phone)
//                            Toast.makeText(context, "It's work!", Toast.LENGTH_SHORT).show()
//                            flagCall = true
//                            saveFlag()
//                            savePhoneWhenClicked(it.creatorInfo.phone)
//
//                        }


                    } else {
                        Log.e("Your Array Response", "Data Null")
                        Toast.makeText(activity!!, "Error fuck you", Toast.LENGTH_SHORT).show()
                    }
                }
            }, object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {
                    Log.e("error is ", "" + error)
                    progressBar.visibility = View.GONE
                    hideSecondBlockWhenXClicked()
                }
            }) {
            //This is for Headers If You Needed
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val params: MutableMap<String, String> =
                    HashMap()
                params["Content-Type"] = "application/json; charset=UTF-8"
                params["Authorization"] =
                    "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9leGFtcGxlLm9yZyIsInVpZCI6MTU2fQ.r9mqVqCX6lrRPoZrP6uWusAlfV_xS3bFWIYnrz4Lq-Q"
                return params
            }

            override fun parseNetworkResponse(response: NetworkResponse?): Response<String> {
                statusCode = response?.statusCode
                return super.parseNetworkResponse(response)
            }

        }

        val queue = Volley.newRequestQueue(activity!!)
        queue.add(request)
        //api end
    }

    //1 post api
    private fun cancelCurrentOrder(id: Int) {

        //параметры которые ты отправляешь
        val parameters: MutableMap<String, Int> =
            HashMap()
        parameters["id"] = id

        val request: JsonObjectRequest = object :
            JsonObjectRequest(
                Method.POST,
                urlPostCancelOrder,
                JSONObject(parameters as Map<*, *>),
                object : Response.Listener<JSONObject> {
                    override fun onResponse(response: JSONObject) {
                        if (response != null) {
                            Toast.makeText(activity!!, "shittt", Toast.LENGTH_SHORT).show()
                        } else {
                            Toast.makeText(activity!!, "faggot", Toast.LENGTH_SHORT).show()
                        }
                    }
                },
                object : Response.ErrorListener {
                    override fun onErrorResponse(error: VolleyError) {
                        Log.e("error is ", "" + error)
                        Toast.makeText(activity!!, error.toString(), Toast.LENGTH_SHORT).show()
                        VolleyLog.v("error is ", "" + error)
                    }
                }) {
            /*override fun getParams(): MutableMap<String, String> {
                return parameters
            }*/

            //This is for Headers If You Needed
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val params: MutableMap<String, String> = HashMap()
                params["Content-Type"] = "application/json; charset=UTF-8"
                params["Authorization"] =
                    "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9leGFtcGxlLm9yZyIsInVpZCI6MTU2fQ.r9mqVqCX6lrRPoZrP6uWusAlfV_xS3bFWIYnrz4Lq-Q"
                return params
            }
        }
        val queue = Volley.newRequestQueue(activity!!)
        VolleyLog.DEBUG = true;
        queue.add(request)
    }

    private fun createNewOrder() {
        //параметры которые ты отправляешь
        val parameters: MutableMap<String, String> =
            HashMap()
        parameters["location_from"] = etAddressFrom.text.toString()
        parameters["location_to"] = etAddressTo.text.toString()
        parameters["price"] = etPriceTaxi.text.toString()
        parameters["date_start"] = tvDate.text.toString()
        parameters["comment"] = etComment.text.toString()
        parameters["time"] = tvTime.text.toString()

        val request: JsonObjectRequest = object :
            JsonObjectRequest(
                Method.POST,
                urlCreateOrder,
                JSONObject(parameters as Map<*, *>),
                Response.Listener<JSONObject> { response ->
                    if (response != null) {
                        Toast.makeText(activity!!, "shittt", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(activity!!, "faggot", Toast.LENGTH_SHORT).show()
                    }
                },
                Response.ErrorListener { error ->
                    Log.e("error is ", "" + error.networkResponse)
                    //Toast.makeText(activity!!, error.networkResponse.statusCode, Toast.LENGTH_SHORT).show()
                    VolleyLog.v("error is ", "" + error)
                    //Toast.makeText(activity!!, "Ошибка какая-то", Toast.LENGTH_SHORT).show()
                    Toast.makeText(activity!!, "вот это ${responseStatus}", Toast.LENGTH_SHORT)
                        .show()
                }) {
            override fun getParams(): MutableMap<String, String> {
                return parameters
            }

            //This is for Headers If You Needed
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val params: MutableMap<String, String> = HashMap()
                params["Content-Type"] = "application/json; charset=UTF-8"
                params["Authorization"] = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9leGFtcGxlLm9yZyIsInVpZCI6MTU2fQ.r9mqVqCX6lrRPoZrP6uWusAlfV_xS3bFWIYnrz4Lq-Q"
                return params
            }

            override fun parseNetworkResponse(response: NetworkResponse?): Response<JSONObject> {
                responseStatus = response?.statusCode
                return super.parseNetworkResponse(response)
            }
        }
        val queue = Volley.newRequestQueue(activity!!)
        VolleyLog.DEBUG = true;
        queue.add(request)
    }

}