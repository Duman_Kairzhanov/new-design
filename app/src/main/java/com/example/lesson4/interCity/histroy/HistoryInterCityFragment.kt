package com.example.lesson4.interCity.histroy

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.lesson4.R
import com.example.lesson4.model.historyDriver.HistoryDriver
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_history_inter_city.*
import java.util.HashMap

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
private val urlHistory:String = "https://test.inqar.kz/v3/ride/history"

/**
 * A simple [Fragment] subclass.
 * Use the [HistoryInterCityFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HistoryInterCityFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_history_inter_city, container, false)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment HistoryInterCityFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            HistoryInterCityFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        swipeRefresh.setOnRefreshListener { apiFunc() }
            //api
        apiFunc()
            //api



    }
    fun apiFunc(){
        swipeRefresh.isRefreshing = true
        var request: StringRequest? = object :
            StringRequest(Request.Method.GET, urlHistory, object : Response.Listener<String?> {
                override fun onResponse(response: String?) {
                    if (response != null) {
                        swipeRefresh.isRefreshing = false
                        Log.d("errApi", response)

                        val gson = Gson()
                        val itemType = object : TypeToken<HistoryDriver>() {}.type
                        val historyDrivers = gson.fromJson<HistoryDriver>(response, itemType)
                        val list = historyDrivers.data
                        val adapter = HistoryDriversAdapter(list, activity!!)
                        rcViewHistoryDrivers.adapter = adapter
                        rcViewHistoryDrivers.hasFixedSize()
                        rcViewHistoryDrivers.layoutManager = LinearLayoutManager(activity!!)

                        adapter.onItemClick = {
//                                startCall(it.creatorInfo.phone)
                        }


                    } else {
                        swipeRefresh.isRefreshing = false
                        Log.e("Your Array Response", "Data Null")
                        Toast.makeText(activity!!, "Error you", Toast.LENGTH_SHORT).show()
                    }
                }
            }, object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {
                    swipeRefresh.isRefreshing = false
                    Log.e("error is ", "" + error)
                }
            }) {
            //This is for Headers If You Needed
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val params: MutableMap<String, String> =
                    HashMap()
                params["Content-Type"] = "application/json; charset=UTF-8"
                params["Authorization"] =
                    "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9leGFtcGxlLm9yZyIsInVpZCI6MTU2fQ.r9mqVqCX6lrRPoZrP6uWusAlfV_xS3bFWIYnrz4Lq-Q"
                return params
            }

        }
        val queue = Volley.newRequestQueue(activity!!)
        VolleyLog.DEBUG = true
        queue.add(request)
    }
}