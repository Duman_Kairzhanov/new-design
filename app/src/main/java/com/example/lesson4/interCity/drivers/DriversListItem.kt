package com.example.lesson4.interCity.drivers

data class DriversListItem (
    var imageDriver:Int,
    var nameDriver:String,
    var ratingDriver:String,
    var priceOfDriver:Int,
    var dateOfTaxi:String,
    var addressFrom:String,
    var addressTo:String,
    var someCommentFromDriver:String,
    var phoneDriver:String
)
