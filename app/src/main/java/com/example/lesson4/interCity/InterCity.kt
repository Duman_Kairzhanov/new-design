package com.example.lesson4.interCity

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.view.Gravity
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.lesson4.R
import com.example.lesson4.orders.CurrentOrdersAndHistoryPageAdapter
import com.example.lesson4.orders.Orders
import com.example.lesson4.profile.MainActivity
import com.example.lesson4.settings.Settings
import com.example.lesson4.support.Support
import com.example.lesson4.terms.TermsOfUse
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.intercity.drawer_layout
import kotlinx.android.synthetic.main.intercity.nav_view
import kotlinx.android.synthetic.main.layout_intercity_page.*
import kotlinx.android.synthetic.main.layout_intercity_page.iv_btn_menu
import kotlinx.android.synthetic.main.layout_intercity_page.tabLayout
import kotlinx.android.synthetic.main.layout_intercity_page.viewpager
import kotlinx.android.synthetic.main.orders_page.*

class InterCity: AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener  {

//    var flagCall = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.intercity)
        nav_view.setNavigationItemSelectedListener(this)
        iv_btn_menu.setOnClickListener {  drawer_layout.openDrawer(Gravity.LEFT) }

        viewpager.adapter =
            InterCityPageAdapter(
                supportFragmentManager
            )
        tabLayout.setupWithViewPager(viewpager)

    }

//    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
//        super.onSaveInstanceState(outState, outPersistentState)
//
//        var flag = flagCall
//        outState.putBoolean("flagCall", flagCall)
//    }
//
//    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
//        super.onRestoreInstanceState(savedInstanceState)
//        savedInstanceState.getBoolean("saveFlag", false)
//    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        clickOnItemNavigation(item)
        return true
    }

    fun toSupport(){
        val intent = Intent(this, Support::class.java)
        startActivity(intent)
    }

    fun toProfile(){
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    fun toTermsOfUse(){
        val intent = Intent(this, TermsOfUse::class.java)
        startActivity(intent)
    }

    fun toSettings(){
        val intent = Intent(this, Settings::class.java)
        startActivity(intent)
    }
    fun toOrders(){
        val intent = Intent(this, Orders::class.java)
        startActivity(intent)
    }
    fun toInterCity() {
        val intent = Intent(this, InterCity::class.java)
        startActivity(intent)
    }

    //клик на  элементы меню
    fun clickOnItemNavigation(item: MenuItem){
        when(item.itemId){
            R.id.nav_support -> { toSupport() }
            R.id.nav_in_city -> { Toast.makeText(this, "City way", Toast.LENGTH_SHORT).show() }
            R.id.nav_between_cities -> { toInterCity() }
            R.id.nav_delivery -> Toast.makeText(this, "Dostavka", Toast.LENGTH_SHORT).show()
            R.id.nav_driver_mode -> Toast.makeText(this, "Driver mode", Toast.LENGTH_SHORT).show()
            R.id.nav_orders -> { toOrders() }
            R.id.nav_profile ->{ toProfile() }
            R.id.nav_settings -> { toSettings() }
            R.id.nav_use_rules -> { toTermsOfUse() }
            R.id.nav_exit -> Toast.makeText(this, "Quit", Toast.LENGTH_SHORT).show()
        }
    }
}