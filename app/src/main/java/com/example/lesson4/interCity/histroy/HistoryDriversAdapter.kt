package com.example.lesson4.interCity.histroy

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.lesson4.R
import com.example.lesson4.model.historyDriver.Data

class HistoryDriversAdapter(listArray: List<com.example.lesson4.model.historyDriver.Data>, context: Context):
    RecyclerView.Adapter<HistoryDriversAdapter.ViewHolder>() {

    var listItemR = listArray
    val contextR = context
    var onItemClick: ((Data) -> Unit)? = null

    inner class ViewHolder(view: View):RecyclerView.ViewHolder(view){
        var imageDriver = view.findViewById<ImageView>(R.id.ivDriverImage)
        var nameDriver = view.findViewById<TextView>(R.id.tvNameOfDriver)
        var ratingDriver = view.findViewById<TextView>(R.id.tvRatingOfDriver)
        var priceOfTaxi = view.findViewById<TextView?>(R.id.tvPriceOfDriver)
        var dateOfTrip = view.findViewById<TextView>(R.id.tvDateOfTrip)
        var addressFrom = view.findViewById<TextView>(R.id.tvAddressFromCity)
        var addressTo = view.findViewById<TextView>(R.id.tvAddressToCity)
        var someCommentDriver = view.findViewById<TextView>(R.id.tvSomeComment)
        var phoneDriver = view.findViewById<TextView>(R.id.tvPhoneDriver)

//        init {
//            itemView.setOnClickListener {
//                onItemClick?.invoke(listItemR[adapterPosition])
//            }
//        }

        fun bind(listItem: Data, context: Context) {
            val radius = context.resources.getDimensionPixelSize(R.dimen.radiusDriver)
            listItem.creator.avatar?.let {
                Glide.with(context)
                    .load(it)
                    .transform(RoundedCorners(radius))
                    .into(imageDriver)
            }

            nameDriver.text = listItem.creator.fio
            ratingDriver.text = listItem.creator.userRating
            priceOfTaxi!!.text= listItem.price
            dateOfTrip.text = listItem.dateStart
            addressFrom.text = listItem.locationFrom
            addressTo.text = listItem.locationTo
            someCommentDriver.text = listItem.comment


        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var inflator = LayoutInflater.from(parent.context)
        return ViewHolder(
            inflator.inflate(R.layout.item_of_rcview_history_drivers, parent, false)
        )
    }

    override fun getItemCount(): Int {
       return listItemR.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var listItem = listItemR.get(position)
        return holder.bind(listItem, holder.itemView.context)
    }
}