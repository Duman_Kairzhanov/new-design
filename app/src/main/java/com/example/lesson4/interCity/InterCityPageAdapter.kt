package com.example.lesson4.interCity

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.lesson4.interCity.toOrder.ToOrderFragment
import com.example.lesson4.interCity.drivers.DriversInterCityFragment
import com.example.lesson4.interCity.histroy.HistoryInterCityFragment

class InterCityPageAdapter(fm: FragmentManager): FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        when(position){
            0 -> { return ToOrderFragment()
            }
            1 -> { return DriversInterCityFragment()
            }
            2 -> { return HistoryInterCityFragment()
            }
            else -> { return ToOrderFragment()
            }
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when(position){
            0 -> { return "Заказать" }
            1 -> { return "Водители" }
            2 -> { return "История" }
        }
        return super.getPageTitle(position)
    }

    override fun getCount(): Int {
        return 3
    }

}