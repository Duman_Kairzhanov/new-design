package com.example.lesson4.model.historyDriver


import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("comment")
    val comment: String,
    @SerializedName("creator")
    val creator: Creator,
    @SerializedName("date_start")
    val dateStart: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("location_from")
    val locationFrom: String,
    @SerializedName("location_to")
    val locationTo: String,
    @SerializedName("price")
    val price: String
)