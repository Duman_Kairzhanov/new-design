package com.example.lesson4.model.drivers


import com.google.gson.annotations.SerializedName

data class CreatorInfo(
    @SerializedName("avatar")
    val avatar: String,
    @SerializedName("driverRating")
    val driverRating: String,
    @SerializedName("fio")
    val fio: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("phone")
    val phone: String
)