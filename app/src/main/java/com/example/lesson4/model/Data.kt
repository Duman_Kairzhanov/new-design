package com.example.lesson4.model


import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("address")
    val address: String,
    @SerializedName("client_delivery_price")
    val clientDeliveryPrice: Int,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("shop")
    val shop: Shop,
    @SerializedName("status")
    val status: Int,
    @SerializedName("statuses")
    val statuses: Statuses,
    @SerializedName("supplyProducts")
    val supplyProducts: List<SupplyProduct>,
    @SerializedName("total_price")
    val totalPrice: Int
)