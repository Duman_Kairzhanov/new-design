package com.example.lesson4.model


import com.google.gson.annotations.SerializedName

data class SupplyProduct(
    @SerializedName("image")
    val image: String,
    @SerializedName("price")
    val price: Int,
    @SerializedName("productName")
    val productName: String,
    @SerializedName("quantity")
    val quantity: Int
)