package com.example.lesson4.model.drivers


import com.google.gson.annotations.SerializedName

data class Self(
    @SerializedName("href")
    val href: String
)