package com.example.lesson4.model.toOrder


import com.google.gson.annotations.SerializedName

data class GetCurrentOrder(
    @SerializedName("data")
    val `data`: Data
)