package com.example.lesson4.model


import com.google.gson.annotations.SerializedName

data class MyOrderInfo(
    @SerializedName("data")
    val `data`: Data
)