package com.example.lesson4.model


import com.google.gson.annotations.SerializedName

data class Statuses(
    @SerializedName("canceled")
    val canceled: String,
    @SerializedName("delivered")
    val delivered: String,
    @SerializedName("in_cook")
    val inCook: String,
    @SerializedName("in_way")
    val inWay: String
)