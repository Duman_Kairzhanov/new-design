package com.example.lesson4.model.drivers


import com.google.gson.annotations.SerializedName

data class Links(
    @SerializedName("self")
    val self: Self
)