package com.example.lesson4.model.drivers


import com.google.gson.annotations.SerializedName

data class Meta(
    @SerializedName("currentPage")
    val currentPage: Int,
    @SerializedName("pageCount")
    val pageCount: Int,
    @SerializedName("perPage")
    val perPage: Int,
    @SerializedName("totalCount")
    val totalCount: Int
)