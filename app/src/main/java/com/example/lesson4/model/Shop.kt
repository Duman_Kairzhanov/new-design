package com.example.lesson4.model


import com.google.gson.annotations.SerializedName

data class Shop(
    @SerializedName("address")
    val address: String,
    @SerializedName("icon")
    val icon: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("phone")
    val phone: String
)