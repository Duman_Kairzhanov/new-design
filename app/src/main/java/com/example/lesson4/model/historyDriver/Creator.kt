package com.example.lesson4.model.historyDriver


import com.google.gson.annotations.SerializedName

data class Creator(
    @SerializedName("avatar")
    val avatar: String,
    @SerializedName("fio")
    val fio: String,
    @SerializedName("userRating")
    val userRating: String
)