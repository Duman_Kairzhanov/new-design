package com.example.lesson4.model.toOrder


import com.google.gson.annotations.SerializedName

data class CreatorInfo(
    @SerializedName("avatar")
    val avatar: String,
    @SerializedName("fio")
    val fio: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("phone")
    val phone: String,
    @SerializedName("userRating")
    val userRating: String
)