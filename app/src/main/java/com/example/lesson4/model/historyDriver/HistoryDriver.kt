package com.example.lesson4.model.historyDriver


import com.google.gson.annotations.SerializedName

data class HistoryDriver(
    @SerializedName("data")
    val `data`: List<Data>,
    @SerializedName("_links")
    val links: Links,
    @SerializedName("_meta")
    val meta: Meta
)