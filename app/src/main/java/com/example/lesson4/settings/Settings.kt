package com.example.lesson4.settings

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.MenuItem
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.example.lesson4.*
import com.example.lesson4.interCity.InterCity
import com.example.lesson4.orders.Orders
import com.example.lesson4.profile.MainActivity
import com.example.lesson4.support.Support
import com.example.lesson4.terms.TermsOfUse
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.settings.*
import kotlinx.android.synthetic.main.settings_page.*

class Settings: Activity(), NavigationView.OnNavigationItemSelectedListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings)
        nav_view.setNavigationItemSelectedListener(this)
        iv_btn_menu.setOnClickListener {
            drawer_layout.openDrawer(Gravity.LEFT)
        }

        clSetLang.setOnClickListener { setLangdialog() }

        loadData()
        swKeepScreen.setOnCheckedChangeListener { compoundButton, b ->
            if(b){
               getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            }
            else{
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            }
            saveData()
        }
    }

    fun toSupport(){
        val intent = Intent(this, Support::class.java)
        startActivity(intent)
    }

    fun toProfile(){
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    fun toTermsOfUse(){
        val intent = Intent(this, TermsOfUse::class.java)
        startActivity(intent)
    }
    fun toSettings(){
        val intent = Intent(this, Settings::class.java)
        startActivity(intent)
    }
    fun toOrders(){
        val intent = Intent(this, Orders::class.java)
        startActivity(intent)
    }
    fun toInterCity() {
        val intent = Intent(this, InterCity::class.java)
        startActivity(intent)
    }

    //клик на  элементы меню
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.nav_support -> { toSupport() }
            R.id.nav_in_city -> {
                Toast.makeText(this, "City way", Toast.LENGTH_SHORT).show() }
            R.id.nav_between_cities -> { toInterCity() }
            R.id.nav_delivery -> Toast.makeText(this, "Dostavka", Toast.LENGTH_SHORT).show()
            R.id.nav_driver_mode -> Toast.makeText(this, "Driver mode", Toast.LENGTH_SHORT).show()
            R.id.nav_orders -> { toOrders() }
            R.id.nav_profile ->{ toProfile() }
            R.id.nav_settings -> { toSettings() }
            R.id.nav_use_rules -> { toTermsOfUse() }
            R.id.nav_exit -> Toast.makeText(this, "Quit", Toast.LENGTH_SHORT).show()
        }
        return true
    }

    // создание диалогового окна  для  выбора  языка
    fun setLangdialog() {
        var setLangDialog = Dialog(this)
        setLangDialog.setContentView(R.layout.select_lang_type)

        setLangDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(setLangDialog.getWindow()?.getAttributes())
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.BOTTOM

        setLangDialog.getWindow()?.setAttributes(lp)


        setLangDialog.show()

        //при клике  на  казахский
        val chooseLangeKaz = setLangDialog.findViewById<TextView>(R.id.tvYesAgreed)
        chooseLangeKaz.setOnClickListener {
            tvLangType.text = chooseLangeKaz.text
            setLangDialog.dismiss()
        }
        //при клике  на  русский
        val chooseLangeRus = setLangDialog.findViewById<TextView>(R.id.tvNoAgreed)
        chooseLangeRus.setOnClickListener {
            tvLangType.text = chooseLangeRus.text
            setLangDialog.dismiss()
        }
        //при клике  на  русский
        val chooseLangeEng = setLangDialog.findViewById<TextView>(R.id.tvEngLang)
        chooseLangeEng.setOnClickListener {
            tvLangType.text = chooseLangeEng.text
            setLangDialog.dismiss()
        }

        val iv_hide = setLangDialog.findViewById<ImageView>(R.id.iv_hide)

        //закрытие  при клике на полоску
        iv_hide?.setOnClickListener { setLangDialog.dismiss() }
    }

    //сохранение состояние при переключении свича
    fun saveData() {
        val sharedPreferences = getSharedPreferences("sharedPref", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.apply{
            putBoolean("BOOLEAN_KEY", swKeepScreen.isChecked)
        }.apply()

        Toast.makeText(this, "Data is saved", Toast.LENGTH_SHORT).show()
    }

    fun loadData() {
        val sharedPreferences = getSharedPreferences("sharedPref", Context.MODE_PRIVATE)
        val savedBoolean = sharedPreferences.getBoolean("BOOLEAN_KEY", false)
        swKeepScreen.isChecked = savedBoolean
    }
}